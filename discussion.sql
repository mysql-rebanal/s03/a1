-- [SECTION] INSERTING Records/Row

-- Syntax: INSERT INTO <table_name>(columns) VALUES (values);

INSERT INTO artists(name) VALUES ("Rivermaya");
INSERT INTO artists(name) VALUES ("Blackpink");
INSERT INTO artists(name) VALUES ("Taylor Swift");
INSERT INTO artists(name) VALUES ("New Jeans");
INSERT INTO artists(name) VALUES ("Bamboo");

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 1);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Born Pink", "2022-09-16", 2);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("The ALbum", "2022-10-02", 2);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Midnights", "2022-10-21", 3);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Tomorrow Becomes Yesterday", "2008-09-28", 5);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Light Peace Love", "2005-01-01",5),("We Stand Alone Together","2007-01-01", 5);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11",3),("Kill this Love","2019-01-01", 2);

--INSERT SONGS

INSERT INTO songs(song_name, length, genre, album_id)
	VALUES ("Snow on the Beach", 256, "Pop", 4),("Anti-Hero", 201, "Pop", 4);

INSERT INTO songs(song_name, length, album_id)
	VALUES ("Bejeweled", 314, 4);
INSERT INTO songs(song_name,album_id)
	VALUES ("Paris", 4);

INSERT INTO songs(song_name, length, genre, album_id)
	VALUES ("Masaya", 450, "Jazz", 6),("Mr. Clay", 357, "Jazz", 6),("Noypi", 700, "OPM", 6);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Himala", 408, "OPM", 1),("Kisapmata", 443, "OPM", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Pink Venom", 306, "K-Pop", 2),("Shut Down", 255, "K-Pop", 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("How You Like That", 300, "K-Pop", 3),("Pretty Savage", 319, "K-Pop", 3);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Attention", 300, "K-Pop", 5),("Hype Boy", 259, "K-Pop", 5);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kalayaan", 338, "OPM", 7),("Muli", 523, "OPM", 7);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Love Story", 355, "Pop", 10),("White Horse", 354, "Pop", 10);

-- [SECTION] READ data from our database
-- SYNTAX:
	-- SELECT <column_name> FROM <table_name>;

SELECT * FROM songs;

SELECT song_name FROM songs;

SELECT song_name, genre FROM songs;

-- To filter the output of the SELECT option

SELECT * FROM albums WHERE artist_id = 3;

-- we can use AND and OR keyword for multiple expressions in the WHERE clause

-- Display the title and length of the OPM songs that are more than 4 minutes

SELECT song_name, length FROM songs WHERE length > 400 AND genre = "OPM";

SELECT song_name, length, genre FROM songs WHERE genre = "Jazz" OR genre = "OPM";

-- [SECTION] UPDATING RECORDS/DATA
-- UPDATE <table_name> SET <column_name> = <value_tobe> WHERE <condition>;

UPDATE songs SET length = 428 WHERE song_name = "Himala";

UPDATE songs SET genre = "Pop" WHERE genre is Null;

-- [SECTION] DELETING RECORDS
-- DELETE FROM <table_name> WHERE <condition>;
-- delete all OPM songs that are more than 4 minutes

DELETE FROM songs WHERE genre = "Original Pinoy Music" AND length > 400;
DELETE FROM songs WHERE genre = "Jazz" AND length > 400;

